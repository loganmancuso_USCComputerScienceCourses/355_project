# Program:'355_Project'

## Last Edit Date: 11-29-2017--11:04:36


* Author/CopyRight: Mancuso, Logan
* Language: Java
    * Following the Google Java Style Guide
        >https://google.github.io/styleguide/javaguide.html

# Building The Project
There are a few simple steps to follow to run this program. The scripts contain error checking so it will not run if you are in the wrong directory. 
* Step 1:
    * navigate to the Working/SourceFiles directory
* Step 2: 
    * type ```../.././Run.sh``` 
        * this script will move the working directory to the test directory, compile the code and then prompt for it to run. 
* Step 3:
    * It will giv you the option to "refresh the date" just type "no/n". This refers to a secondary script that allows me to change the last edited date on all files recursivly. 
* Step 4:
    * the last prompt will ask if you want to proceed with the execution type "yes/y" and it will handle all the command line arguments. 
    * 
If you need to rerun the program and do not want to recompile just navigate back to the Working/SourceFiles directory and type ``` ../../Executable/./zeExecute.sh ```